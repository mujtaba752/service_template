const mongoose = require('mongoose');

const linkSchema = mongoose.Schema(
  {
    link: {
      type: String,
      required: true,
      //unique: true,
      //dropDups: true,
    },
    sslInfo: {
      type: Object,
      default: {},
    },
    dateNow: {
      type: Number,
      required: true,
      default: 0,
    },
    status: {
      type: String,
      enum: ['pending', 'running', 'completed', 'crashed'],
      default: 'pending',
    },
    domainId: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Domain' }],
  },
  {
    timestamps: true,
  }
);

const Link = mongoose.model('Link', linkSchema);

module.exports = Link;
