const mongoose = require('mongoose');

const domainSchema = mongoose.Schema({
  domain: {
    type: String,
    required: true,
    unique: true,
    dropDups: true
  },
  summary: {
    type: Array,
    default: []
  },
  dateNow: {
    type: Number,
    required: true,
    default: 0,
  },
  sslInfo: {
    type: Object,
    default: {},
  },
  status: {
    type: String,
    enum: ['pending', 'running', 'completed', 'crashed'],
    default: 'pending',
  },
}, {
  timestamps: true
});

const Domain = mongoose.model('Domain', domainSchema);

module.exports = Domain;
