const { OK, UNPROCESSABLE_ENTITY, NOT_FOUND } = require('http-status');
// const axios = require('axios');
const { isValidUrl } = require('../services/api.service');
const domainModel = require('../models/domain.model');
const linkModel = require('../models/link.model');

function root(req, res, next) {
  res
    .json({
      msg: 'please select api version',
      apis: ['v1'],
    })
    .status(OK);
}

function v1(req, res, next) {
  res
    .status(OK)
    .json({
      msg: 'use sub-apis',
      apis: ['POST /api/v1/check', 'GET /api/v1/status', 'GET /api/v1/result'],
    });
}

async function check(req, res, next) {
  const { domain, dateNow } = req.body;
  if (typeof dateNow !== 'undefined' || typeof domain !== 'undefined') {

    res
      .status(OK)
      .json({
        msg: 'scan scheduled',
        data: null,
      });

    try {
      domain = new URL(domain).hostname;
    } catch (err) {
      console.log("Aleardy Hostname")
    };

    let clientDomain = await domainModel.findOne({
      domain, dateNow
    });
    if (!clientDomain) {
      clientDomain = new domainModel({
        domain, dateNow
      });
      clientDomain = await clientDomain.save();
    }

    // data = await sslDetails(domain);
    // console.log(data);

    // clientDomain.sslInfo = data;

    clientDomain.status = 'completed';
    clientDomain = await clientDomain.save();
  } else {
    console.error('no urls to scan');

    res
      .status(UNPROCESSABLE_ENTITY)
      .json({
        msg: 'no urls or invalid datenow',
        data: null,
      });
  }
}

async function getDomainResult(req, res, next) {
  let { domain, dateNow } = req.body;
  const clientDomain = await domainModel.findOne({
    domain, dateNow
  });
  if (clientDomain) {
    const data = clientDomain.sslInfo;

    res
      .status(OK)
      .json({
        msg: 'domain found',
        data,
      });
  } else {
    res
      .status(NOT_FOUND)
      .json({
        msg: 'no such domain or invalid date provided',
        data: null,
      });
  }
}

async function getDomainStatus(req, res, next) {
  const { domain, dateNow } = req.body;
  const clientDomain = await domainModel.findOne({
    domain, dateNow
  });
  if (clientDomain) {
    const data = clientDomain.status;
    res
      .status(OK)
      .json({
        msg: 'domain with date found',
        data
      });
  } else {
    res
      .status(NOT_FOUND)
      .json({
        msg: 'no such domain',
        data: null,
      });
  }
}

module.exports = {
  root,
  v1,
  check,
  getDomainResult,
  getDomainStatus,
};
