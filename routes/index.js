const express = require('express');
const router = express.Router();

router.get('/', async (req, res, next) => {
  res.status(200).json({ name: 'SSL Checker API' });
});

router.get('/_health', async (req, res, next) => {
  res.status(200).json({ status: 'OK', name: "ssl-checker ms" });
});

module.exports = router;
