const express = require('express');
const router = express.Router();

const { root, v1, check, getDomainStatus, getDomainResult } = require('../controllers/api.controller');

router.get('/', root);

router.get('/v1', v1);

router.post('/v1/check', check);
router.get('/v1/status', getDomainStatus);
router.get('/v1/result', getDomainResult);

module.exports = router;
